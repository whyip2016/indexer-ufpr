# README

Este é o README para a aplicação indexer.

## Pré-requisitos

Antes de executar a aplicação, certifique-se de ter os seguintes pré-requisitos instalados:

1. **Java 11**: A aplicação requer o Java Development Kit (JDK) versão 11. Você pode baixar e instalar o JDK 11 a partir do [site oficial da Oracle](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) ou usar uma distribuição como o OpenJDK.

2. **Maven**: A aplicação utiliza o Maven como gerenciador de dependências e construção do projeto. Você pode baixar o Maven a partir do [site oficial do Apache Maven](https://maven.apache.org/download.cgi) e seguir as instruções de instalação.

## Como Executar a Aplicação

Siga os passos abaixo para executar a aplicação:

1. Abra um terminal ou prompt de comando.

2. Navegue até o diretório raiz do projeto.

3. Execute o seguinte comando para compilar e construir o projeto:

    ```bash
    mvn clean compile assembly:single
    ```

4. Após a conclusão bem-sucedida da construção, execute o seguinte comando para iniciar a aplicação:

    ```bash
    java -jar target/indexer-1.0-SNAPSHOT-jar-with-dependencies.jar --freq N ARQUIVO
    ```

   Certifique-se de substituir o ARQUIVO pelo nome real do arquivo, incluindo a path completa do arquivo.
   Ex: "users/Document/sampleFile.txt"

    ```bash
    java -jar target/indexer-1.0-SNAPSHOT-jar-with-dependencies.jar --freq-word PALAVRA ARQUIVO
    ```
   
    ```bash
    java -jar target/indexer-1.0-SNAPSHOT-jar-with-dependencies.jar --search TERMO ARQUIVO [ARQUIVO ...]
    ```



Obrigado por usar a aplicação indexer!

William Yip
GRR 20185480
