package ufpr.indexer;


import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class Main implements Runnable {

    @Option(names = "--freq", arity = "2", description = "\n --freq N ARQUIVO \n Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.\n")
   private String[] freqValues;
    @Option(names = "--freq-word", arity = "2", description = "--freq-word PALAVRA ARQUIVO \n Exibe o número de ocorrências de PALAVRA em ARQUIVO. \n")
    private String[] freqWordValues;

    @Option(names = "--search", arity = "2..*", description = "--search TERMO ARQUIVO [ARQUIVO ...] \n Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por \n" +
            "    TERMO. A listagem é apresentada em ordem descrescente de relevância. \n" +
            "    TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre \n" +
            "    àspas.\n")
    private String[] search;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
    @Override
    public void run() {
        try {
            if (Objects.nonNull(freqValues)) {
                Integer topN = Integer.valueOf(freqValues[0]);
                String fileName = freqValues[1];
                TopNWords.getTopNWords(fileName, topN);
            }
            if (Objects.nonNull(freqWordValues)) {
                String word = freqWordValues[0];
                String file = freqWordValues[1];
                TopNWords.getWordFrequency(word, file);
            }
            if (Objects.nonNull(search)) {
                String searchTerm = search[0];
                String[] filePaths = Arrays.copyOfRange(search, 1, search.length);

                SearchTerm.mostRelevantFilesByTerm(searchTerm, filePaths);
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}