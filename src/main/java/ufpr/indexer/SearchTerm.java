package ufpr.indexer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SearchTerm {

    public static void main(String[] args) throws IOException {
        String[] paths = {"/media/william-yip/Seagate Expansion Drive1/indexer-tests/8mb.txt"};
        String searchTerm = "Lucius Seneca";

        mostRelevantFilesByTerm(searchTerm, paths);
    }

    public static void mostRelevantFilesByTerm(String term, String[] filePaths) throws IOException {
        Map<String, Double> relevanceByFile = new HashMap<>();
        Double totalSearchTermOccurrences = 0.0;

        for (String file: filePaths) {
            FileInfo fileInfo = countOccurrences(term.toLowerCase(), file);
            // term frequency -> (total occurrences of a term in file X) / (total words in file X)
            Double termFrequency = fileInfo.getTermFreq() / fileInfo.getTotalWords();
            relevanceByFile.put(file, termFrequency.doubleValue());

            if (fileInfo.getTermFreq() > 0)
                totalSearchTermOccurrences++;
        }

        // inverse frequency -> log (total num of documents) / (num of docs that the term is present)
        double inverseFrequency = Math.log10(filePaths.length / totalSearchTermOccurrences);

        PriorityQueue<Map.Entry<String, Double>> descendingResult = new PriorityQueue<>((val1, val2) -> val1.getValue().compareTo(val2.getValue()) * -1);

        for (Map.Entry<String, Double> entry : relevanceByFile.entrySet()) {
            // how relevant is that file -> tf * if
            entry.setValue(entry.getValue().doubleValue() * inverseFrequency);
            descendingResult.add(entry);
        }

        ArrayList<Map.Entry<String, Double>> answer = new ArrayList<>();
        answer.add(descendingResult.poll());

        System.out.println(prettifyAnswer(answer, term));
    }

    private static String prettifyAnswer(List<Map.Entry<String, Double>> answer, String term) {
        System.out.println("\nArquivos mais importantes pelo termo buscado '" + term + "' e o seu TFIDF(t,d,D)");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < answer.size(); i++) {
            int position = i + 1;
            sb.append(String.format("%d - %s: %,.010f \n", position, answer.get(i).getKey(), answer.get(i).getValue()));
        }

        return sb.toString();
    }

    private static FileInfo countOccurrences(String searchTerm, String filePath) throws IOException {
        FileInfo fileInfo = new FileInfo();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String currentLine;

            while ((currentLine = reader.readLine()) != null) {
                countOccurrencesInLine(currentLine.toLowerCase(), searchTerm, fileInfo);
            }

        }

        return fileInfo;
    }

    private static void countOccurrencesInLine(String line, String searchTerm, FileInfo fileInfo) {
        if (line.indexOf(searchTerm) != -1)
            fileInfo.setTermFreq(fileInfo.getTermFreq() + 1);

        String[] words = line.split("\\s+"); // Assume que as palavras são separadas por espaços em branco
        for (String word : words) {
            word = word.toLowerCase().replaceAll("[^a-zA-Z-]", ""); // Converte para minúsculas e remove caracteres não alfabéticos menos o "-"

            if (word.length() > 2)
                fileInfo.setTotalWords(fileInfo.getTotalWords() + 1);

        }

    }

}
