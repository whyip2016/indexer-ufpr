package ufpr.indexer;

public class FileInfo {
    private Double totalWords = 0.0;
    private Double termFreq = 0.0;

    public Double getTotalWords() {
        return totalWords;
    }

    public void setTotalWords(Double totalWords) {
        this.totalWords = totalWords;
    }

    public Double getTermFreq() {
        return termFreq;
    }

    public void setTermFreq(Double termFreq) {
        this.termFreq = termFreq;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "totalWords=" + totalWords +
                ", termFreq=" + termFreq +
                '}';
    }

}
