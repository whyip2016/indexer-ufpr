package ufpr.indexer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TopNWords {

    public static void main(String[] args) {
        String filePath = "/media/william-yip/Seagate Expansion Drive1/indexer-tests/8mb.txt";
        int topN = 10;
        long before = System.currentTimeMillis();

        try {
//                getTopNWords(filePath, topN);
            getWordFrequency("bem-vindo", filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Execution took " + (System.currentTimeMillis() - before) / 1000 + " seconds. ");
        }
    }

    public static void getWordFrequency(String word, String filePath) throws IOException {
        Map<String, Integer> wordFrequency = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                processLine(line, wordFrequency);
        }
    }

        System.out.println("\nNúmero de ocorrências da palavra "+word +": "+wordFrequency.getOrDefault(word,0));
}

    public static Queue<Map.Entry<String, Integer>> getTopNWords(String filePath, int topN) throws IOException {
        Map<String, Integer> wordFrequency = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                processLine(line, wordFrequency);
            }
        }

        Queue<Map.Entry<String, Integer>> topNWordsFromMap = getTopNWordsFromMap(wordFrequency, topN);

        Stack<Map.Entry<String, Integer>> stack = new Stack<>();

        while (!topNWordsFromMap.isEmpty()) {
            stack.push(topNWordsFromMap.poll());
        }

        System.out.println(String.format("\nTop %d palavras mais frequentes no arquivo buscado e seu número de ocorrências.\n", topN));
        while (!stack.isEmpty()) {
            System.out.println(stack.pop().toString().replace("=", ": "));
        }

        return topNWordsFromMap;
    }

    private static void processLine(String line, Map<String, Integer> wordFrequency) {
        String[] words = line.toLowerCase().split("\\s+"); // Assume que as palavras são separadas por espaços em branco
        for (String word : words) {
            word = word.toLowerCase().replaceAll("[^a-zA-Z-]", ""); // Converte para minúsculas e remove caracteres não alfabéticos menos o "-"
            if (word.length() > 2) {
                wordFrequency.put(word, wordFrequency.getOrDefault(word, 0) + 1);
            }
        }
    }

    private static Queue<Map.Entry<String, Integer>> getTopNWordsFromMap(Map<String, Integer> wordFrequency, int topN) {
        Queue<Map.Entry<String, Integer>> topNWords = new PriorityQueue<>(
                (entry1, entry2) -> entry1.getValue().compareTo(entry2.getValue())
        );

        for (Map.Entry<String, Integer> entry : wordFrequency.entrySet()) {
            topNWords.offer(entry);
            if (topNWords.size() > topN) {
                topNWords.poll(); // Remover a palavra menos frequente do heap
            }
        }

        return topNWords;
    }
}
